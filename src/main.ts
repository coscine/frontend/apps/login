import Vue from "vue";
import LoginApp from "./LoginApp.vue";
import jQuery from "jquery";
import BootstrapVue from "bootstrap-vue";
import "@itcenter-layout/bootstrap/dist/css/rwth-theme.css";
import "@itcenter-layout/bootstrap/dist/css/material-icons.css";
import "@itcenter-layout/masterpage/dist/css/itcenter-masterdesign-masterpage.css";
import VueI18n from "vue-i18n";

Vue.config.productionTip = false;

Vue.use(BootstrapVue);
Vue.use(VueI18n);

let localeValue = ((navigator as any).language ||
  (navigator as any).userLanguage) as string;
if (localeValue.indexOf("de") !== -1) {
  localeValue = "de";
} else {
  localeValue = "en";
}

jQuery(() => {
  const i18n = new VueI18n({
    locale: localeValue, // set locale
    messages: coscine.i18n.login, // set locale messages
    silentFallbackWarn: true,
  });

  new Vue({
    render: (h) => h(LoginApp),
    i18n,
  }).$mount("#loginpage");
});
