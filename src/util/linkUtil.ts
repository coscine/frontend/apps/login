function getReturnUrlParam() {
  const hookupElement = document.getElementById("loginpage");
  const returnUrlParameters =
    hookupElement != null ? hookupElement.getAttribute("returnUrl") : "";
  return returnUrlParameters;
}

const returnUrlParams = getReturnUrlParam();

export function getReturnUrl(method: string, entityId = "") {
  let returnUrl =
    "/coscine/api/Coscine.Api.STS/" +
    method +
    "/login?returnUrl=" +
    returnUrlParams;
  if (entityId !== "") {
    returnUrl += "&entityId=" + entityId;
  }
  return encodeURI(returnUrl);
}

export function getMergeReturnUrl() {
  return getReturnUrl("Merge");
}

export function getTOSReturnUrl() {
  return encodeURI("" + getReturnUrlParam());
}

export function getAccountReturnUrl() {
  return getReturnUrl("Account");
}

export function getIdpUrl() {
  const hookupElement = document.getElementById("loginpage");
  const idpUrl =
    hookupElement != null ? hookupElement.getAttribute("IdpUrl") : "";
  return idpUrl;
}

export function getORCiDUrl() {
  const hookupElement = document.getElementById("loginpage");
  const orcidUrlString = (
    hookupElement != null ? hookupElement.getAttribute("orcidUrl") : ""
  ) as string;
  const orcidUrl = encodeURI(orcidUrlString);
  return orcidUrl;
}

export function getIsLogout() {
  const urlParams = new URLSearchParams(window.location.search);
  return urlParams.has("logout");
}

export function getIsTOS() {
  const urlParams = new URLSearchParams(window.location.search);
  return urlParams.has("tos");
}

export function getLoggedInWithShibboleth() {
  const urlParams = new URLSearchParams(window.location.search);
  return urlParams.has("loggedInWithShibboleth");
}
