declare module "*.vue" {
  import Vue from "vue";
  export default Vue;
}

declare module "@coscine/api-connection";
declare module "@coscine/component-library";
declare module "vue-markdown";
declare module "vue-multiselect";
declare module "@itcenter-layout/bootstrap";
declare module "*.png" {
  const value: any;
  export default value;
}
declare module "*.svg" {
  const value: any;
  export default value;
}
