const parser = require('fast-xml-parser');
const axios = require('axios');
const fs = require('fs').promises;

async function main(){
    const response = await axios.get('https://www.aai.dfn.de/fileadmin/metadata/dfn-aai-basic-metadata.xml');
    const allEntities = parser.parse(response.data, {ignoreAttributes : false});
    const obj = allEntities.EntitiesDescriptor.EntityDescriptor
        .filter(e => e.IDPSSODescriptor)
        .reduce((o, e) => {
            return {...o , ...entityToObject(e)};
    }, {});
    await fs.writeFile("src/data/dfnaai.json", JSON.stringify(obj, null, 2));
}

function entityToObject(e){
    const o = {};
    o[e['@_entityID']] = {
        DisplayName : getDisplayName(e),
        Logo : getLogo(e),
    }
    return o;
}

function getDisplayName(entity){
    const dns = makeArray(entity.IDPSSODescriptor.Extensions['mdui:UIInfo']['mdui:DisplayName']);
    const o = {};
    for(let i=0; i<dns.length; i++){
        const dn = dns[i];
        o[dn['@_xml:lang']] = dn['#text'];
    }
    return o;
}

function getLogo(entity){
    const logos = makeArray(entity.IDPSSODescriptor.Extensions['mdui:UIInfo']['mdui:Logo']);
    let logo = undefined;
    let size = -1;
    for(let i=0; i<logos.length; i++){
        const s = logos[i]['@_height'] * logos[i]['@_width'] || 0;
        if(s > size){
            size = s;
            logo = logos[i]['#text'];
        }
    }
    return logo;
}

function makeArray(x){
    return  x ? Array.isArray(x) ? x : [ x ] : [];
}

main();
